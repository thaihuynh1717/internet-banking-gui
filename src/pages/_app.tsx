import "../styles/globals.css";
import type { AppProps } from "next/app";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { io } from "socket.io-client";
import { useEffect, useState } from "react";
import "react-toastify/dist/ReactToastify.css";
import { toast, ToastContainer } from "react-toastify";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { AUTH_KEY } from "@/hooks/useAuth";
import { useRouter } from "next/router";

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      initialData: null,
    },
  },
});
const socket = io("ws://wannacloud.online:13006");
export default function App({ Component, pageProps }: AppProps) {
  const [isConnected, setIsConnected] = useState(socket.connected);
  const [lastMessage, setLastMessage] = useState(null);

  useEffect(() => {
    if (isConnected === true) {
      console.info("Yay! Every thing worked!");
      // toast.success("Yay! Every thing worked!");
    }
    if (isConnected === false) {
      // console.error("Uh oh. Something went wrong.");
      // toast.error("Uh oh. Something went wrong.");
    }
  }, [isConnected]);

  useEffect(() => {
    socket.on("connect", () => {
      setIsConnected(true);
    });
    socket.on("disconnect", () => {
      setIsConnected(false);
    });
    socket.on("message", (data) => {
      console.log("message: ", data);
      setLastMessage(data);
    });
    return () => {
      socket.off("connect");
      socket.off("disconnect");
      socket.off("message");
    };
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <Component {...pageProps} />
      <ToastContainer
        position="bottom-left"
        autoClose={1000}
        hideProgressBar={true}
        newestOnTop={true}
        draggable={false}
        closeOnClick
      />
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}
