import Layout from "@/components/Layout";
import styles from "@/styles/Home.module.scss";
import HomeIcon from "@mui/icons-material/Home";
import WindowIcon from "@mui/icons-material/Window";
import MessageIcon from "@mui/icons-material/Message";
import PaymentIcon from "@mui/icons-material/Payment";
import CurrencyExchangeIcon from "@mui/icons-material/CurrencyExchange";
import RequestQuoteIcon from "@mui/icons-material/RequestQuote";
import HistoryIcon from "@mui/icons-material/History";
import {
  Avatar,
  Card,
  List,
  ListItemButton,
  ListItemIcon,
  Stack,
  Tab,
  Tabs,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import { useGetCustomer } from "@/hooks/useCutomer";
import { useGetAuth } from "@/hooks/useAuth";
import { useEffect, useState } from "react";
import Accounts from "@/components/Accounts";
import { IsLoading } from "@/components/IsLoading";

const options = [
  { text: "Bảng tin", icon: HomeIcon },
  { text: "Chuyển khoản", icon: CurrencyExchangeIcon },
  // { text: "Thanh toán", icon: PaymentIcon },
  // { text: "Tin nhắn", icon: MessageIcon },
  { text: "Nhắc nợ", icon: RequestQuoteIcon },
  { text: "Lịch sử", icon: HistoryIcon },
  // { text: "Dịch vụ", icon: WindowIcon },
];
const Home = () => {
  const [focused, setFocused] = useState(0);
  const router = useRouter();
  const auth = useGetAuth();
  const getCustomer = useGetCustomer();
  const toggleOption = (index: number) => setFocused(index);
  useEffect(() => {
    if (!auth.data) router.push("/users/sign-in");
    if (auth.data) getCustomer.refetch();
  }, [auth.data, getCustomer, router]);

  if (getCustomer.data)
    return (
      <Layout>
        <main className="flex justify-center items-center h-screen w-screen">
          <div className={styles.neumorphism}>
            <div className="flex flex-row flex-1 p-8">
              <div className="flex flex-col w-60">
                <div className="flex flex-row justify-start items-center">
                  <Avatar
                    src="/app-logo.svg"
                    alt=""
                    sx={{ width: 24, height: 24 }}
                  />
                  <h6 className="mx-4">{getCustomer.data?.full_name}</h6>
                </div>
                <List>
                  {options.map((option, index) => (
                    <ListItemButton
                      key={option.text + "-" + index}
                      sx={{
                        padding: 1,
                        opacity: focused === index ? 1 : 0.4,
                      }}
                      onClick={() => toggleOption(index)}
                    >
                      <ListItemIcon>{<option.icon />}</ListItemIcon>
                      {option.text}
                    </ListItemButton>
                  ))}
                </List>
              </div>
              <div className={styles.content}>
                {focused === 0 && <Accounts />}
              </div>
            </div>
          </div>
        </main>
      </Layout>
    );
  return <IsLoading />;
};

export default Home;
