import axios from "axios";
import { AUTH_KEY } from "@/hooks/useAuth";

// function getCookieRefreshToken() {
//   try {
//     const auth = getCookieAuth();
//     return auth.refreshToken;
//   } catch (error) {
//     return '';
//   }
// }

// function getExpiredToken() {
//   const now = new Date().getTime() / 1000;
//   const auth = getCookieAuth();
//   const timeToken = new Date(auth.expiredTokenIn).getTime() / 1000;
//   const isExpired = timeToken - now < 1 ? true : false;
//   return isExpired;
// }
// function getExpiredRefreshToken() {
//   const now = new Date().getTime() / 1000;
//   const auth = getCookieAuth();
//   const timeRefreshToken =
//     new Date(auth.expiredRefreshTokenIn).getTime() / 1000;
//   const isExpired = timeRefreshToken - now < 1 ? true : false;
//   return isExpired;
// }

const baseURL = 'https://internet-banking-server.wannacloud.online/';
const instance = axios.create({
  baseURL: baseURL,
  timeout: 5 * 1000,
  headers: {
    "Content-Type": "application/json",
  },
});

const getAuth = () => JSON.parse(sessionStorage.getItem(AUTH_KEY) || "null");

// Add a request interceptor
instance.interceptors.request.use(
  function (config: any) {
    // Do something before request is sent
    const auth = getAuth();
    if (auth) {
      config.headers = {
        Authorization: `${auth.token_type} ${auth.access_token}`,
      };
      // const isExpiredToken = getExpiredToken();
      // const isExpiredRefreshToken = getExpiredRefreshToken();
      // if (isExpiredToken === false) {
      //     return req;
      // }
      // if (isExpiredToken === true) {
      //     if (isExpiredRefreshToken === true) {
      //         const response = await axios.post(`${baseURL}/refresh-tokens`, {
      //             refreshToken: localAuth.refreshToken,
      //         });
      //         if (response.status === 200) {
      //             localStorage.setItem('auth', JSON.stringify(response.data));
      //             req.headers.Authorization = `${response.data.type} ${response.data.accessToken}`;
      //         }
      //         if (response.status === 404) {
      //             sessionHelper.removeAuth();
      //         }
      //     }
      //     if (isExpiredRefreshToken === false) {
      //         sessionHelper.removeAuth();
      //     }
      // }
    }
    return config;
  },
  function (error: any) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  function (response: any) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (response.data || response.data.data) {
      return response.data.data;
    }
    return response;
  },
  function (error: any) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.error(error);

    if (error.response && error.response.data) {
      try {
        const message = error.response.data.message;

        if (message) error.response.data.message = message;
        return Promise.reject(error.response.data);
      } catch (error1) {
        return Promise.reject(error.response.data);
      }
    }
    return Promise.reject(error);
  }
);

export class GetTokenDto {
  email: string | undefined;
  password: string | undefined;
}
export class GetCustomerDto {
  customer_id: string | undefined;
}

class HttpService {
  async getToken(params: GetTokenDto): Promise<any> {
    return await instance.post(`api/auth/customer/token`, params);
  }

  async getCustomerInfo(params: GetCustomerDto): Promise<any> {
    return await instance.get(`api/customer/${params.customer_id}`);
  }

  async getCustomerAccount(params: GetCustomerDto): Promise<any> {
    return await instance.get(`api/customer/${params.customer_id}/account`);
  }
}



export default new HttpService();
