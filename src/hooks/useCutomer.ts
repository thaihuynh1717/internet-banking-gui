import HttpService, { GetCustomerDto } from "@/services/HttpService";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import { useGetAuth } from "./useAuth";

export const CUSTOMER_KEY = "CUSTOMER_KEY";
export const CUSTOMER_ACCOUNT_KEY = "CUSTOMER_ACCOUNT_KEY";

export function useGetCustomer() {
  const auth = useGetAuth();
  return useQuery({
    queryKey: [CUSTOMER_KEY],
    queryFn: async () =>
      await HttpService.getCustomerInfo({
        customer_id: auth.data.customer_id,
      }),
    onError: async (error) => {
      sessionStorage.clear();
      await auth.refetch();
    },
  });
}
export function useGetCustomerAccount() {
  const auth = useGetAuth();
  return useQuery({
    queryKey: [CUSTOMER_ACCOUNT_KEY],
    queryFn: async () => {
      try {
        if (!auth.data) return [];
        const response = await HttpService.getCustomerAccount({
          customer_id: auth.data.customer_id,
        });
        return response;
      } catch (error: any) {
        toast.error(error.message);
        sessionStorage.clear();
        await auth.refetch();
      }
      return [];
    },
  });
}
