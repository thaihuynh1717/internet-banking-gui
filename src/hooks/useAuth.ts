import HttpClient, { GetTokenDto } from "@/services/HttpService";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { Id, toast } from "react-toastify";
import { useRouter } from "next/router";

export const AUTH_KEY = "AUTH_KEY";

export function useGetAuth() {
  return useQuery({
    queryKey: [AUTH_KEY],
    queryFn: () => JSON.parse(sessionStorage.getItem(AUTH_KEY) || "null"),
  });
}

export function useGetToken() {
  const auth = useGetAuth();
  let toastId: Id;
  return useMutation({
    mutationFn: (params: GetTokenDto) => HttpClient.getToken(params),
    onMutate: () => {
      sessionStorage.clear();
      toastId = toast.loading("Đang xác thực");
    },
    onSuccess: async (data) => {
      sessionStorage.setItem(AUTH_KEY, JSON.stringify(data));
      toast.done(toastId);
      toast.success("Xác thực thành công");
      await auth.refetch();
    },
    onError: async (error: any) => {
      toast.done(toastId);
      toast.error(error.message);
      sessionStorage.clear();
      await auth.refetch();
    },
  });
}

export function useRemoveAuth() {
  const auth = useGetAuth();
  const router = useRouter();
  return useMutation({
    mutationFn: async () => sessionStorage.clear(),
    onSuccess: async (data) => {
      await auth.refetch();
      router.push("/");
    },
    onError: (error) => toast.error(JSON.stringify(error)),
  });
}
