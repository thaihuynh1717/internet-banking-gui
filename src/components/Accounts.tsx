import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Image from "next/image";
import { useGetCustomerAccount } from "@/hooks/useCutomer";
import { IsLoading } from "./IsLoading";
import { Chip, Stack, Typography } from "@mui/material";

function createData(
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number
) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];

export default function BasicTable() {
  const getCustomerAccount = useGetCustomerAccount();

  if (getCustomerAccount.data)
    return (
      <>
        <Typography sx={{ marginBottom: 2 }}>
          Tài khoản ({getCustomerAccount.data.length})
        </Typography>
        <TableContainer elevation={4} component={Paper} className="rounded-3xl">
          <Table sx={{ width: "100%" }} aria-label="simple table">
            <TableBody>
              {getCustomerAccount.data.map((row: any, index: number) => (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    <Image
                      src={"/credit-card.png"}
                      alt={""}
                      width={64}
                      height={32}
                    />
                  </TableCell>
                  <TableCell align="right">
                    <Stack spacing={1}>
                      <div className="flex flex-row items-center justify-between">
                        <Typography className="font-bold">
                          Sô tài khoản: {row.customer_account_id}
                        </Typography>
                        {row.active === 1 && (
                          <Chip
                            variant="outlined"
                            size="small"
                            label="Mặc định"
                            className="text-blue-500 border-blue-500"
                          />
                        )}
                      </div>
                      <div className="flex flex-row items-center justify-start">
                        <Typography className="text-gray-300">
                          Số dư khả dụng: {row.balance} VND
                        </Typography>
                      </div>
                    </Stack>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </>
    );

  return <IsLoading />;
}
