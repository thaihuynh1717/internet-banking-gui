import Image from "next/image";

export const IsLoading = () => (
  <main className="flex justify-center items-center h-screen">
    <Image src={"/app-logo.svg"} alt={""} width={96} height={96} />
  </main>
);
